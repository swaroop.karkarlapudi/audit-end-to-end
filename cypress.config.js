const { defineConfig } = require("cypress");
const { readFileSync } = require('fs');
const mongo = require('cypress-mongodb');

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      mongo.configurePlugin(on);
      if(config.env.name) {
        const envName = config.env.name;
        const text = readFileSync(`./environments/${envName}.json`);
        const values = JSON.parse(text.toString());
        config.baseUrl = values.baseUrl;
        config.env = {
          ...values
        }
        return config;
      }
    },
    retries: {
      runMode: 0
    }
  },
  env: {
    baseUrl: 'https://audit-dev.payments-dev-future.dwpcloud.uk',
    mongodb: {
      uri: "mongodb+srv://kraken-tester:DontBreakIt_12345@dev-cluster.ddf8q.mongodb.net",
      database: "payments-audit-dev",
      collection: "auditEvent"
  }
  },
  video: false,
});
