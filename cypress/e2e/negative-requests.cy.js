/// <reference types="cypress" />

describe('audit-capture-negative-tests', function() {

    describe('mandatory fields', function() {
        it('POST audit event with missing mandatory properties', function() {
            cy.request({
                method: 'POST',
                url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
                body: {},
                failOnStatusCode: false
            }).then((response) => {
                expect(response.status).equal(400);
                expect(response.body.message).equal(`request/body must have required property 'eventDateTime', request/body must have required property 'dataType', request/body must have required property 'eventId', request/body must have required property 'userId', request/body must have required property 'productId', request/body must have required property 'activityType', request/body must have required property 'outcomeCode'`);
            })
        });
    });

    describe('eventDateTime', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing eventDateTime', function() {
            delete this.data['eventDateTime'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'eventDateTime\'`);
            })
        });
    
        it('POST audit event with eventDateTime value as an invalid date', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventDateTime':'1'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventDateTime must match format "date-time"`);
            })
        });
    
        it('POST audit event with eventDateTime value as a number', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventDateTime':1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventDateTime must be string`);
            })
        });
    
        it('POST audit event with empty eventDateTime value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventDateTime':""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventDateTime must match format "date-time"`);
            })
        });
    
        it('POST audit event with eventDateTime value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventDateTime':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventDateTime must be string`);
            })
        });
    });

    describe('dataType', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing dataType', function() {
            delete this.data['dataType'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'dataType\'`);
            })
        });
    
        it('POST audit event with dataType value as a number', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dataType':1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dataType must be string`);
            })
        });
    
        it('POST audit event with empty dataType value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dataType':""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dataType must NOT have fewer than 1 characters`);
            })
        });
    
        it('POST audit event with dataType value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dataType':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dataType must be string`);
            })
        });
        it('POST audit event with dataType value greater than 6 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dataType':'abcefgh'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dataType must NOT have more than 6 characters`);
            })
        });
    });

    describe('eventId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing eventId', function() {
            delete this.data['eventId'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'eventId\'`);
            })
        });
        it('POST audit event with eventId value in incorrect format', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventId':'test1234'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventId must match pattern "^DAPA[0-9]{4}$"`);
            })
        });
    
        it('POST audit event with eventId value as a number', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventId':1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventId must be string`);
            })
        });
    
        it('POST audit event with empty eventId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventId':""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventId must match pattern "^DAPA[0-9]{4}$"`);
            })
        });
    
        it('POST audit event with eventId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventId':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventId must be string`);
            })
        });
        it('POST audit event with eventId value greater than 8 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'eventId':'DAPA41466'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/eventId must match pattern "^DAPA[0-9]{4}$"`);
            })
        });
    });

    describe('userId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing userId', function() {
            delete this.data['userId'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'userId\'`);
            })
        });
    
        it('POST audit event with userId value as a number', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'userId':1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/userId must be string`);
            })
        });
    
        it('POST audit event with empty userId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'userId':""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/userId must NOT have fewer than 1 characters`);
            })
        });
    
        it('POST audit event with userId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'userId':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/userId must be string`);
            })
        });
        it('POST audit event with userId value greater than 200 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{
                'userId':'ELMIIzUscDKMQiFbpwgzizGRYdppmpMAwbgbvQpszAoVXiMYXxGIhXFcfwtNiYmZQKEmmtEXpdUHjAbbXlZuGVSwqqDqitFAduDIKxqbpaskWBTJdPIXPlUodfsbrSupLVjtfAocXMyIovIEdhVtwIOgdKSHsAexKStDoZEhuZctvfbgnedvsozDBjEWdgkOoErcvrvHH'
            }),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/userId must NOT have more than 200 characters`);
            })
        });
    });

    describe('productId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing productId', function() {
            delete this.data['productId'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'productId\'`);
            })
        });
        it('POST audit event with productId value as a number', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'productId':1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/productId must be string`);
            })
        });
    
        it('POST audit event with empty productId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'productId':""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/productId must NOT have fewer than 1 characters`);
            })
        });
    
        it('POST audit event with productId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'productId':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/productId must be string`);
            })
        });
        it('POST audit event with productId value greater than 12 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'productId':'123STR^&*MVG9'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/productId must NOT have more than 12 characters`);
            })
        });
    });

    describe('activityType', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing activityType', function() {
            delete this.data['activityType'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'activityType\'`);
            })
        });
        it('POST audit event with activityType value as a number', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'activityType':1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/activityType must be string, request/body/activityType must be equal to one of the allowed values: CREATE, READ, UPDATE, DELETE`);
            })
        });
    
        it('POST audit event with empty activityType value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'activityType':""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/activityType must be equal to one of the allowed values: CREATE, READ, UPDATE, DELETE`);
            })
        });
    
        it('POST audit event with activityType value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'activityType':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/activityType must be string, request/body/activityType must be equal to one of the allowed values: CREATE, READ, UPDATE, DELETE`);
            })
        });
        it('POST audit event with activityType value greater than 6 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'activityType':'abcdefg'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/activityType must be equal to one of the allowed values: CREATE, READ, UPDATE, DELETE`);
            })
        });
        it('POST audit event with activityType value as a lowercase create', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'activityType':'create'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/activityType must be equal to one of the allowed values: CREATE, READ, UPDATE, DELETE`);
            })
        });
        it('POST audit event with activityType value which is not CREATE, READ, UPDATE and DELETE', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'activityType':'test'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/activityType must be equal to one of the allowed values: CREATE, READ, UPDATE, DELETE`);
            })
        });
    });

    describe('outcomeCode', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });
        it('POST audit event with missing outcomeCode', function() {
            delete this.data['outcomeCode'];
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: this.data,
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body must have required property \'outcomeCode\'`);
            })
        });
        it('POST audit event with outcomeCode value as a string', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeCode':'1'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeCode must be integer`);
            })
        });
    
        it('POST audit event with outcomeCode value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeCode':null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeCode must be integer`);
            })
        });
        it('POST audit event with outcomeCode value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeCode': 2}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeCode must be <= 1`);
            })
        });
    });

    describe('nino', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with nino value with invalid format', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'nino': 'BG'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/nino must match pattern "^((?!(BG|GB|KN|NK|NT|TN|ZZ)|([DFIUV])[A-Z]|[A-Z]([DFIOUV]))[A-Z]{2})[0-9]{6}[A-D]?$"`);
            })
        });

        it('POST audit event with nino value with greater than 6 digits', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'nino': 'NH0587133'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/nino must match pattern "^((?!(BG|GB|KN|NK|NT|TN|ZZ)|([DFIUV])[A-Z]|[A-Z]([DFIOUV]))[A-Z]{2})[0-9]{6}[A-D]?$"`);
            })
        });

        it('POST audit event with nino value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'nino': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/nino must be string`);
            })
        });

        it('POST audit event with nino value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'nino': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/nino must be string`);
            })
        });

        it('POST audit event with empty nino value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'nino': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/nino must match pattern "^((?!(BG|GB|KN|NK|NT|TN|ZZ)|([DFIUV])[A-Z]|[A-Z]([DFIOUV]))[A-Z]{2})[0-9]{6}[A-D]?$"`);
            })
        });
    });

    describe('ipAddress', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with ipAddress value with invalid format', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'ipAddress': '123456'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/ipAddress must match pattern "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"`);
            })
        });

        it('POST audit event with ipAddress value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'ipAddress': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/ipAddress must be string`);
            })
        });

        it('POST audit event with ipAddress value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'ipAddress': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/ipAddress must be string`);
            })
        });

        it('POST audit event with empty ipAddress value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'ipAddress': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/ipAddress must match pattern "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"`);
            })
        });
    });

    describe('consumerId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with consumerId value with value greater than 32 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerId': '9RNK76FVTduKUeFnviTqAnMwB08CzTQ9D'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerId must NOT have more than 32 characters`);
            })
        });

        it('POST audit event with consumerId value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerId': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerId must be string`);
            })
        });

        it('POST audit event with consumerId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerId': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerId must be string`);
            })
        });

        it('POST audit event with empty consumerId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerId': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerId must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('screenId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with screenId value with value greater than 32 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'screenId': '9RNK76FVTduKUeFnviTqAnMwB08CzTQ9D'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/screenId must NOT have more than 32 characters`);
            })
        });

        it('POST audit event with screenId value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'screenId': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/screenId must be string`);
            })
        });

        it('POST audit event with screenId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'screenId': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/screenId must be string`);
            })
        });

        it('POST audit event with empty screenId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'screenId': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/screenId must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('deviceName', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with deviceName value with value greater than 10 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'deviceName': '9RNK7909876'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/deviceName must NOT have more than 10 characters`);
            })
        });

        it('POST audit event with deviceName value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'deviceName': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/deviceName must be string`);
            })
        });

        it('POST audit event with deviceName value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'deviceName': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/deviceName must be string`);
            })
        });

        it('POST audit event with empty deviceName value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'deviceName': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/deviceName must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('macAddress', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with macAddress value with invalid format', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'macAddress': '9RNK7909876'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/macAddress must match pattern "^[a-fA-F0-9]{2}(:[a-fA-F0-9]{2}){5}$"`);
            })
        });

        it('POST audit event with macAddress value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'macAddress': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/macAddress must be string`);
            })
        });

        it('POST audit event with macAddress value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'macAddress': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/macAddress must be string`);
            })
        });

        it('POST audit event with empty macAddress value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'macAddress': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/macAddress must match pattern "^[a-fA-F0-9]{2}(:[a-fA-F0-9]{2}){5}$"`);
            })
        });
    });

    describe('browserType', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with browserType value with value greater than 200 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'browserType': '1i3kurl42p6f2uifhqJoPOFtjk0vjkSvFSm7Kne6UwdAwfMuSFU3vzqfx2MAKD76ObrmC9dTFFH2NK0N3hl55nlIS6mQMDmBEAbMKaJnKPAx708ajosnQ0Xgcnd6FsFsfGkMohfN3W8ghc05X51xnbAwDPevYRU5y7ymfjh72qJLr6Ma2a0Tk7zT8SZguemeSoJ4ZyxZs'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/browserType must NOT have more than 200 characters`);
            })
        });

        it('POST audit event with browserType value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'browserType': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/browserType must be string`);
            })
        });

        it('POST audit event with browserType value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'browserType': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/browserType must be string`);
            })
        });

        it('POST audit event with empty browserType value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'browserType': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/browserType must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('firstName', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with firstName value with value greater than 70 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'firstName': 'mJzRzYcxK2dr5eROyo3r0Jxjgq45KsAJLrgnkXSmNuAhoJL1nfzNr4pAxnjh42nrDyi6JDE'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/firstName must NOT have more than 70 characters`);
            })
        });

        it('POST audit event with firstName value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'firstName': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/firstName must be string`);
            })
        });

        it('POST audit event with firstName value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'firstName': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/firstName must be string`);
            })
        });

        it('POST audit event with empty firstName value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'firstName': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/firstName must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('lastName', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with lastName value with value greater than 35 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'lastName': 'M3Uqn2BbvtMfBCGPxLyILMZhKxcfvnAw243W'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/lastName must NOT have more than 35 characters`);
            })
        });

        it('POST audit event with lastName value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'lastName': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/lastName must be string`);
            })
        });

        it('POST audit event with lastName value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'lastName': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/lastName must be string`);
            })
        });

        it('POST audit event with empty lastName value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'lastName': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/lastName must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('consumerGuid', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with consumerGuid value with value greater than 64 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerGuid': 'Iy3taydBwMKtwLSrKfKr73bDs8DXh7XimNKc8YvVvONA0LnLA5tnCQUG3rX4j6Pl3'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerGuid must NOT have more than 64 characters`);
            })
        });

        it('POST audit event with consumerGuid value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerGuid': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerGuid must be string`);
            })
        });

        it('POST audit event with consumerGuid value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerGuid': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerGuid must be string`);
            })
        });

        it('POST audit event with empty consumerGuid value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'consumerGuid': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/consumerGuid must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('dwpGuid', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with dwpGuid value with value greater than 64 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dwpGuid': 'Iy3taydBwMKtwLSrKfKr73bDs8DXh7XimNKc8YvVvONA0LnLA5tnCQUG3rX4j6Pl3'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dwpGuid must NOT have more than 64 characters`);
            })
        });

        it('POST audit event with dwpGuid value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dwpGuid': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dwpGuid must be string`);
            })
        });

        it('POST audit event with dwpGuid value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dwpGuid': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dwpGuid must be string`);
            })
        });

        it('POST audit event with empty dwpGuid value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'dwpGuid': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/dwpGuid must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('outcomeText', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with outcomeText value with value greater than 200 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeText': '1i3kurl42p6f2uifhqJoPOFtjk0vjkSvFSm7Kne6UwdAwfMuSFU3vzqfx2MAKD76ObrmC9dTFFH2NK0N3hl55nlIS6mQMDmBEAbMKaJnKPAx708ajosnQ0Xgcnd6FsFsfGkMohfN3W8ghc05X51xnbAwDPevYRU5y7ymfjh72qJLr6Ma2a0Tk7zT8SZguemeSoJ4ZyxZs'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeText must NOT have more than 200 characters`);
            })
        });

        it('POST audit event with outcomeText value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeText': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeText must be string`);
            })
        });

        it('POST audit event with outcomeText value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeText': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeText must be string`);
            })
        });

        it('POST audit event with empty outcomeText value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'outcomeText': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/outcomeText must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('correlationId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with correlationId value with value greater than 64 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'correlationId': 'Iy3taydBwMKtwLSrKfKr73bDs8DXh7XimNKc8YvVvONA0LnLA5tnCQUG3rX4j6Pl3'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/correlationId must NOT have more than 64 characters`);
            })
        });

        it('POST audit event with correlationId value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'correlationId': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/correlationId must be string`);
            })
        });

        it('POST audit event with correlationId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'correlationId': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/correlationId must be string`);
            })
        });

        it('POST audit event with empty correlationId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'correlationId': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/correlationId must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('sessionId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with sessionId value with value greater than 64 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'sessionId': 'Iy3taydBwMKtwLSrKfKr73bDs8DXh7XimNKc8YvVvONA0LnLA5tnCQUG3rX4j6Pl3'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/sessionId must NOT have more than 64 characters`);
            })
        });

        it('POST audit event with sessionId value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'sessionId': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/sessionId must be string`);
            })
        });

        it('POST audit event with sessionId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'sessionId': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/sessionId must be string`);
            })
        });

        it('POST audit event with empty sessionId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'sessionId': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/sessionId must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('interfaceId', function() {
        beforeEach('Get test data', function() {
            cy.fixture('audit-event').then(function(data){
              this.data = data;
            });
        });

        it('POST audit event with interfaceId value with value greater than 32 characters', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'interfaceId': 'Iy3taydBwMKtwLSrKfKr73bDs8DXh7Xim'}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/interfaceId must NOT have more than 32 characters`);
            })
        });

        it('POST audit event with interfaceId value as num', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'interfaceId': 1}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/interfaceId must be string`);
            })
        });

        it('POST audit event with interfaceId value as null', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'interfaceId': null}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/interfaceId must be string`);
            })
        });

        it('POST audit event with empty interfaceId value', function() {
            cy.request({
              method: 'POST',
              url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
              body: Object.assign(this.data,{'interfaceId': ""}),
              failOnStatusCode: false
            }).then((response) => {
              expect(response.status).equal(400);
              expect(response.body.message).equal(`request/body/interfaceId must NOT have fewer than 1 characters`);
            })
        });
    });

    describe('additionalAttributes', function() {
        describe('attributeName', function() {
            beforeEach('Get test data', function() {
                cy.fixture('audit-event').then(function(data){
                  this.data = data;
                });
            });
            it('POST audit event with missing attributeName', function() {
                delete this.data.additionalAttributes[0]['attributeName'];
                cy.request({
                  method: 'POST',
                  url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
                  body: this.data,
                  failOnStatusCode: false
                }).then((response) => {
                  expect(response.status).equal(400);
                  expect(response.body.message).equal(`request/body/additionalAttributes/0 must have required property 'attributeName'`);
                })
            })
        })
    })
});