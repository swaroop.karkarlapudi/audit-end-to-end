/// <reference types="cypress" />
const mongoose = require('mongoose');

describe('audit-capture-positive-tests', function() {
  before('Get test data', function() {
    cy.fixture('audit-event').then(function(data){
      this.data = data;
    });
  });
  it('POST audit event', function() {
    cy.request({
      method: 'POST',
      url: `${Cypress.env('baseUrl')}/audit-capture/v1/audit/auditEvent`,
      body: this.data,
    }).then((response) => {
      expect(response.status).equal(201);
      expect(response.body.acknowledged).equal(true);
      expect(response.body.insertedId).to.be.a('string');
      global.insertedId = response.body.insertedId;
    })
  });

  it('check event is stored in db', function() {
    const objectId = mongoose.Types.ObjectId(global.insertedId);
    const query = { "_id": objectId };
    cy.findOne(query).then(res => {
      cy.log(res);
      expect(res).to.not.equal(null);
      expect(res.eventId).equal('DAPA4146');
    });
  });

  it('check event is purged from db', function() {
    cy.wait(65000);
    const objectId = mongoose.Types.ObjectId(global.insertedId);
    const query = { "_id": objectId };
    cy.findOne(query).then(res => {
      expect(res).equal(null);
    });
  });
});